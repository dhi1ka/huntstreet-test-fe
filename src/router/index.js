import { createRouter, createWebHistory } from 'vue-router';
import Home from '@/views/Home.vue'
import About from '@/views/About.vue'
import Form from '@/views/Form.vue'
import GetLink from '@/views/GetLink.vue'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    component: About
  },
  {
    path: '/form',
    name: 'Form',
    component: Form
  },
  {
    path: '/getlink',
    name: 'GetLink',
    component: GetLink
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
